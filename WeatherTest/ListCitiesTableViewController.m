//
//  ListCitiesTableViewController.m
//  WeatherTest
//
//  Created by Alex on 22.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "ListCitiesTableViewController.h"
#import "APIManager.h"
#import "City.h"
#import "Utils.h"
#import "SVProgressHUD.h"
#import "VKSdk.h"
#import "CityTableViewCell.h"

@interface ListCitiesTableViewController () <FCAlertViewDelegate>

@property (strong, nonatomic) NSMutableArray* citiesArray;
@property (assign, nonatomic) NSInteger countAnimatedCells;
@property (assign, nonatomic) NSInteger currentRow;
@property (assign, nonatomic) BOOL dataIsLoading;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation ListCitiesTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initializeVariables];
    
    [self addBunchRandomCities];
    
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.citiesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    //NSLog(@"cellForRowAtIndexPath: { %ld, %ld }", indexPath.section, indexPath.row);
    
    static NSString* identifier = @"CityCell";
    
    CityTableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        cell = [[CityTableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
    }
    
    City* city = self.citiesArray[indexPath.row];
    
    [cell makeCell:cell andIndexPathRow:indexPath.row andCity:city];
    
    if (indexPath.row >=  self.countAnimatedCells) {
        self.countAnimatedCells = indexPath.row;
        
        [Utils makeAnimation:cell];

    }
   
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int row= (int)indexPath.row ;
    
    if(self.dataIsLoading == NO &&  row == self.citiesArray.count-1) {
        //NSLog(@"time to add DATA");
        [self addBunchRandomCities];
    }
        
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.currentRow = indexPath.row;
    
    [Utils alertView:self andTitle:@"Post on wall?"];
}

#pragma mark - Private methods

- (void) initializeVariables {
    
    self.citiesArray = [NSMutableArray new];
    self.countAnimatedCells = 0;
    self.dataIsLoading = NO;
    self.currentRow = 0;
    
}

- (void) addBunchRandomCities {
    
    if (![Utils connectedToInternet]) {
        NSLog(@"no internet");
    } else {
        NSLog(@"internet is ok");
        self.dataIsLoading = YES;
        
        [Utils SVProgressHUDStart];
        
        APDispatchToBackgroundQueue(^{
            
            [APIManager getForecastForBunchCities:^(NSMutableArray *listCities) {
                
                [self.citiesArray addObjectsFromArray:listCities];
                
                //NSLog(@"add amount of cities - %ld", self.citiesArray.count);
                
                NSMutableArray *newPaths = [NSMutableArray array];
                
                for (NSUInteger i = [self.citiesArray count] - [listCities count]; i < [self.citiesArray count]; i++) {
                    [newPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
                }
                
                APDispatchToMainQueue(^{
                    [SVProgressHUD dismiss];
                    [self.tableView beginUpdates];
                    [self.tableView insertRowsAtIndexPaths:newPaths withRowAnimation:UITableViewRowAnimationFade];
                    [self.tableView endUpdates];
                    self.dataIsLoading = NO;
                    [self.tableView scrollToRowAtIndexPath:newPaths[0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
                });
                
            } onFailure:^(NSError *error) {
                NSLog(@"error - %@", error.localizedDescription);
            }];
            
            
        });
    }
}

#pragma mark FCAlertView delegate

- (void) FCAlertView:(FCAlertView *)alertView clickedButtonIndex:(NSInteger)index buttonTitle:(NSString *)title {

    if ([title isEqualToString:@"NO"]) {
        NSLog(@"NO");
    }
}

- (void)FCAlertDoneButtonClicked:(FCAlertView *)alertView {
    NSLog(@"YES");
    
    City* city = [self.citiesArray objectAtIndex:self.currentRow];
    
    NSString* message = [NSString stringWithFormat:@"in the city %@ of the United Kingdom current weather is - %.2f ºC, %@ ", city.nameCity, city.temp, city.descriptionWeather];
    
    [APIManager wallPostWithMessage:message
                          onSuccess:^{
                              NSLog(@"succes");
                          }
                          onFailure:^(NSError *error) {
                              NSLog(@"error - %@", error.localizedDescription);
                          }];
}

@end
