//
//  City.m
//  WeatherTest
//
//  Created by Alex on 23.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "City.h"

@implementation City

- (instancetype)initWithDictionary:(NSDictionary*) dict
{
    self = [super init];
    if (self) {
        if ([dict count] != 0) {
            self.nameCity = (NSString*)[dict objectForKey:@"name"];
            NSDictionary* main = (NSDictionary*)[dict objectForKey:@"main"];
            self.temp = [[main objectForKey:@"temp"] floatValue] - 273.15; //cause we need Celcius
            
            NSArray* weather = (NSArray*)[dict objectForKey:@"weather"];
            NSDictionary* weatherZero = [weather objectAtIndex:0];
            
            self.descriptionWeather = (NSString*)[weatherZero objectForKey:@"description"];
            NSString* iconName = (NSString*)[weatherZero objectForKey:@"icon"];
            self.icon = [UIImage imageNamed:iconName];
        }
    }
    return self;
}

@end
