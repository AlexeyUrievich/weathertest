//
//  AuthViewController.h
//  WeatherTest
//
//  Created by Alex on 23.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VKSdk.h"
#import "Utils.h"

@interface AuthViewController : UIViewController <VKSdkDelegate>

@end
