//
//  City.h
//  WeatherTest
//
//  Created by Alex on 23.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface City : NSObject

@property (strong, nonatomic) NSString* nameCity;
@property (assign, nonatomic) CGFloat temp;
@property (strong, nonatomic) NSString* descriptionWeather;
@property (strong, nonatomic) UIImage* icon;

- (instancetype)initWithDictionary:(NSDictionary*) dict;

@end

