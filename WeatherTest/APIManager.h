//
//  APIManager.h
//  WeatherTest
//
//  Created by Alex on 23.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "City.h"
#import "VKSdk.h"

@interface APIManager : NSObject

+ (void) getForecastForBunchCities:(void(^)(NSMutableArray* listCities)) success
                                   onFailure:(void(^)(NSError* error)) failure;

+ (void) wallPostWithMessage:(NSString*) message
                   onSuccess:(void(^)()) success
                   onFailure:(void(^)(NSError* error)) failure;

@end
