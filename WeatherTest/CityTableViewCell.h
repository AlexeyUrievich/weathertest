//
//  CityTableViewCell.h
//  WeatherTest
//
//  Created by Alex on 25.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "City.h"

@interface CityTableViewCell : UITableViewCell

- (CityTableViewCell*) makeCell:(CityTableViewCell*) cell
                andIndexPathRow:(NSInteger) indexPathRow
                        andCity:(City*) city;

@end
