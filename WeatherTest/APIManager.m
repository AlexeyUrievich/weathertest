//
//  APIManager.m
//  WeatherTest
//
//  Created by Alex on 23.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "APIManager.h"

NSString* const URL_CITYNAMES = @"http://raw.githubusercontent.com/David-Haim/CountriesToCitiesJSON/master/countriesToCities.json";
NSString* const URL_OPENWEATHERAMP = @"http://api.openweathermap.org/data/2.5/weather?q";
//NSString* const APIKEY = @"&APPID=ef47af49c9a6dd32fa339c693d63692e"; this API was banned for overlimit -)
NSString* const APIKEY = @"&APPID=8e100450075e4d9ec4c3a556e4d96fb4";

@implementation APIManager

+ (NSArray*) getCitiesList {
    
    NSURL* citiesNamesURL = [[NSURL alloc] initWithString:URL_CITYNAMES];
    
    NSData* citiesNamesData = [[NSData alloc] initWithContentsOfURL:citiesNamesURL];
    
    NSDictionary* citiesNamesJson = [NSJSONSerialization JSONObjectWithData:citiesNamesData
                                                                    options:kNilOptions error:nil];
    
    NSArray* citiesArray = [citiesNamesJson objectForKey:@"United Kingdom"];
    
    return citiesArray;
    
}
+ (void) wallPostWithMessage:(NSString*) message
                   onSuccess:(void(^)()) success
                   onFailure:(void(^)(NSError* error)) failure {
    
    VKRequest* request = [VKRequest requestWithMethod:@"wall.post"
                                           parameters:@{VK_API_OWNER_ID : [VKSdk accessToken].userId,
                                                        @"message" : message,
                                                        VK_API_ACCESS_TOKEN : [VKSdk accessToken].accessToken,
                                                        @"v"               : @"5.59"}
                          ];
    
    request.waitUntilDone = YES;
    
    [request executeWithResultBlock:^(VKResponse *response) {
        
        if (success) {
            success();
        }
    }                    errorBlock:^(NSError *error) {
        if (failure) {
            failure(error);
        }
    }];
}
+ (void) getForecastForBunchCities:(void(^)(NSMutableArray* listCities)) success
                         onFailure:(void(^)(NSError* error)) failure {
    
    NSArray* listCitiesArray = [self getCitiesList];
    
    NSInteger amountOfCitiesInBunch = 15;
    
    NSMutableArray* newBunchOfCitiesWithForecast = [NSMutableArray new];
    
    int i = 0;
    
    while ( i < amountOfCitiesInBunch) {
        
        int randomCity = arc4random_uniform((int)listCitiesArray.count);
        
        NSString* stringForHttp = [NSString stringWithFormat:@"%@=%@,uk%@",URL_OPENWEATHERAMP,[listCitiesArray objectAtIndex:randomCity],APIKEY];
        
        NSURL* stringURL = [[NSURL alloc] initWithString:stringForHttp];
        
        NSData* weatherData = [[NSData alloc] initWithContentsOfURL:stringURL];
        
        if (weatherData != nil) {
            
            i = i + 1;
            
            NSError *jsonError;
            
            NSDictionary* weatherJson = [NSJSONSerialization JSONObjectWithData:weatherData
                                                                        options:NSJSONReadingMutableContainers error:&jsonError];
            
            if (weatherJson == nil) {
                if (failure) {
                    failure(jsonError);
                }
                break;
            } else {
                City* city = [[City alloc] initWithDictionary:weatherJson];
                
                [newBunchOfCitiesWithForecast addObject:city];
                
                //NSLog(@"amountOfCitiesInBunch - %ld, name - %@, temp %.2f, descr - %@, icon - %@", amountOfCitiesInBunch, city.nameCity, city.temp, city.descriptionWeather, city.icon);
            }
        }
    }
    if (success) {
        success(newBunchOfCitiesWithForecast);
    }
}

@end
