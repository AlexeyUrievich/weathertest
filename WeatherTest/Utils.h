//
//  Utils.h
//  WeatherTest
//
//  Created by Alex on 23.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import "FCAlertView.h"
#import "CityTableViewCell.h"
#import "Reachability.h"


@interface Utils : NSObject

typedef void(^CodeBlock)(void);
void APDispatchToBackgroundQueue(CodeBlock);
void APDispatchToMainQueue(CodeBlock);
void APDispatchAfter(CGFloat, CodeBlock);

+ (void) SVProgressHUDStart;

+ (void) makePause;

+ (void) alertView:(UIViewController*) viewController andTitle:(NSString*) title;

+ (void) makeAnimation:(CityTableViewCell*) cell;

+ (BOOL)connectedToInternet;

@end
