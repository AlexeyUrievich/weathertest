//
//  AuthViewController.m
//  WeatherTest
//
//  Created by Alex on 23.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "AuthViewController.h"

static NSString *const APP_NUMBER = @"5791612";
static NSString *const NEXT_CONTROLLER_SEGUE_ID = @"START_WORK";
static NSArray *SCOPE = nil;

@interface AuthViewController ()  <VKSdkUIDelegate>

@end

@implementation AuthViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
  
        // https://vk.com/dev/permissions
    
        SCOPE = @[VK_PER_WALL];
    
        // start of work of SDK, smth like a Shared Instance
        
        [[VKSdk initializeWithAppId:APP_NUMBER] registerDelegate:self]; // number ID of application TopPolls
        [[VKSdk instance] setUiDelegate:self];
        
        // start session, and if user is not authorised yet - then we show screen of authorize (native VC), other way - go to next View Controller for work
        
        [VKSdk wakeUpSession:SCOPE completeBlock:^(VKAuthorizationState state, NSError *error) {
            
            if (state == VKAuthorizationAuthorized) {
                [self startWorking];
            } else if (error) {
                NSLog(@"Пожалуйста проверьте Ваше Интернет соединение");
            }
        }];
    }
- (void)startWorking {
        
        [self performSegueWithIdentifier:NEXT_CONTROLLER_SEGUE_ID sender:self];
}

#pragma  mark - Actions
    
- (IBAction)authorize:(UIButton *)sender {
        
        // get new access token
        
        [Utils makePause];
        
        [VKSdk authorize:SCOPE];
        
        //  this method makes procedure of authorize + realization method of delegate - (void)vkSdkShouldPresentViewController:(UIViewController *)controller {}
}
    
#pragma mark - @protocol VKSdkDelegate
    
- (void)vkSdkTokenHasExpired:(VKAccessToken *)expiredToken {
        
        [self authorize:nil];
}
    
- (void)vkSdkAccessAuthorizationFinishedWithResult:(VKAuthorizationResult *)result {
        
  if (result.token) {
            
            [self startWorking];
        } else if (result.error) {
            NSLog(@"В доступе отказано");
     
        }
    }

- (void)vkSdkUserAuthorizationFailed {
        
        NSLog(@"В доступе отказано");
        [self.navigationController popToRootViewControllerAnimated:YES];
}
    
#pragma mark - @protocol VKSdkUIDelegate
    
- (void)vkSdkNeedCaptchaEnter:(VKError *)captchaError {
 
        VKCaptchaViewController *vc = [VKCaptchaViewController captchaControllerWithError:captchaError];
        [vc presentIn:self.navigationController.topViewController];
        NSLog(@"Введите пожалуйста текст" );
      
}
    
- (void)vkSdkShouldPresentViewController:(UIViewController *)controller {
        
        NSLog(@"@protocol VKSdkUIDelegate vkSdkShouldPresentViewController ");
        
        [self.navigationController.topViewController presentViewController:controller animated:YES completion:nil];
}

@end
