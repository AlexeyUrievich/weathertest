//
//  Utils.m
//  WeatherTest
//
//  Created by Alex on 23.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "Utils.h"

@implementation Utils

void APDispatchAfter(CGFloat time, CodeBlock block)
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t) (time * NSEC_PER_SEC)), dispatch_get_main_queue(), block);
}

void APDispatchToMainQueue(CodeBlock block)
{
    if ([NSThread isMainThread])
    {
        if (block) block();
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           if (block) block();
                       });
    }
}

void APDispatchToBackgroundQueue(CodeBlock block)
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^
                   {
                       if (block) block();
                   });
}

+ (void) SVProgressHUDStart {
    
    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    [SVProgressHUD setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.3]];
    [SVProgressHUD setForegroundColor:[UIColor whiteColor]];
    [SVProgressHUD show];
    
}

+ (void) makePause {
    
    [[UIApplication sharedApplication]  beginIgnoringInteractionEvents];
    double delayInSeconds = 0.2f;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if ([[UIApplication sharedApplication] isIgnoringInteractionEvents]) {
            [[UIApplication sharedApplication] endIgnoringInteractionEvents];
        }
    });
}

+ (void) alertView:(UIViewController*) viewController andTitle:(NSString*) title{
    
    FCAlertView *alert = [[FCAlertView alloc] init];
    
    alert.delegate = (id<FCAlertViewDelegate>)viewController;
    
    alert.colorScheme = alert.flatBlue;
    
    alert.cornerRadius = 4;
    
    [alert showAlertInView:viewController
                 withTitle:title
              withSubtitle:nil
           withCustomImage:[UIImage imageNamed:@"01d"]
       withDoneButtonTitle:@"YES"
                andButtons:@[@"NO"]];
}

+ (void) makeAnimation:(CityTableViewCell*) cell {
    
    UIView *myView = cell.contentView;
    
    [myView setAlpha:0.0];
    
    CGAffineTransform scale = CGAffineTransformMakeScale(0, 0);
    
    myView.transform = scale;
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options: UIViewAnimationOptionCurveEaseInOut animations:^
     {
         [myView setAlpha:1.0];
         
         CGAffineTransform scale = CGAffineTransformMakeScale(1, 1);
         
         myView.transform = scale;
         
     } completion:nil];
    
}

+ (BOOL)connectedToInternet
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

@end
