//
//  CityTableViewCell.m
//  WeatherTest
//
//  Created by Alex on 25.12.16.
//  Copyright © 2016 Alex. All rights reserved.
//

#import "CityTableViewCell.h"

@implementation CityTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - methods

- (CityTableViewCell*) makeCell:(CityTableViewCell*) cell
                andIndexPathRow:(NSInteger) indexPathRow
                        andCity:(City*) city{
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@ - %.2f ºC", city.nameCity, city.temp];
    
    UIFont* font = [UIFont fontWithName:@"Arial" size:14];
    
    cell.textLabel.font = font;
    
    cell.textLabel.numberOfLines = 2;
    
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    cell.textLabel.textColor = [UIColor blackColor];
    
    cell.detailTextLabel.font = [UIFont fontWithName:@"Arial" size:12];
    
    cell.detailTextLabel.textColor = [UIColor blackColor];
    
    cell.detailTextLabel.text = city.descriptionWeather;
    
    cell.detailTextLabel.numberOfLines = 2;
    
    cell.textLabel.adjustsFontSizeToFitWidth = YES;
    
    cell.imageView.image = city.icon;
    
    return cell;
}

@end
